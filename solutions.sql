

#insert users

INSERT INTO users(email, password, datetime_created)
values
("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-01-01 01:00:00"),
("janesmith@gmail.com", "passwordC", "2021-01-01 01:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-01-01 01:00:00"),
("johndoe@gmail.com", "passwordE", "2021-01-01 01:00:00");

#insert to posts
INSERT INTO posts(title, content, datetime_created, user_id)
values
("First Code", "Hello World!", "2021-01-02 01:00:00", 1),
("Second Code", "Hello Earth!", "2021-01-02 02:00:00", 1),
("Third Code", "welcome to Mars!", "2021-01-02 03:00:00", 2),
("Fourth Code", "bye bye solar system", "2021-01-02 04:00:00", 4);

# get all post of user id = 1;
SELECT * FROM posts WHERE user_id = 1; 

#get user email and date creation

SELECT email, datetime_created FROM users;

#update hello earth 
UPDATE posts SET content = "Hello to the people of the earth" WHERE id = 2;

#delete user using email 

DELETE FROM users WHERE email = "johndoe@gmail.com";